import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import ReactNativeBiometrics from 'react-native-biometrics';
import { AsyncStorage } from 'react-native';

export default function App() {

  function login() {
    ReactNativeBiometrics.simplePrompt({promptMessage: "Confirmer l'emprunte digitale"})
        .then(async (resultObject) => {
            const { success } = resultObject
            if (success) {
            ReactNativeBiometrics.biometricKeysExist()
                .then(async (resultObject) => {
                    const { publicKey } = resultObject
                    const jsonUser = await AsyncStorage.getItem('user')
                    const user = JSON.parse(jsonUser)
                    setUserPrenom(user.prenom)
                    setUserNom(user.nom)
                })
                .catch(() => {
                    console.log("failed")
                })
            } else {
                console.log('user cancelled biometric prompt')
            }
        })
        .catch(() => {
           console.log('biometrics failed')
        })
  }

  function register() {
        ReactNativeBiometrics.simplePrompt({promptMessage: "Confirmer l'emprunte digitale"})
                                      .then((resultObject) => {
                                        const { success } = resultObject

                                        if (success) {
                                           ReactNativeBiometrics.createKeys('Confirm fingerprint')
                                                            .then((resultObject) => {
                                                              const { publicKey } = resultObject

                                                              let data = {}
                                                              data.prenom = prenom
                                                              data.nom = nom
                                                              const jsonData = JSON.stringify(data)

                                                              AsyncStorage.setItem(
                                                                    'user',
                                                                    jsonData
                                                                  );

                                                              setIsRegistered(true)
                                                            })
                                                            .catch(() => {
                                                              console.log("failed")
                                                            })
                                        } else {
                                          console.log('user cancelled biometric prompt')
                                        }
                                      })
                                      .catch(() => {
                                        console.log('biometrics failed')
                                      })
  }

  const [prenom, setPrenom] = React.useState('');
  const [nom, setNom] = React.useState('');

  const [userPrenom, setUserPrenom] = React.useState('');
  const [userNom, setUserNom] = React.useState('');

  const [isRegistered, setIsRegistered] = React.useState(false)


  return (
    <View style={styles.container}>
    { isRegistered &&
        <View style={styles.user}>
            <Text>Prénom : <Text>{userPrenom}</Text></Text>
            <Text>Nom : <Text>{userNom}</Text></Text>
        </View>
    }
    { isRegistered &&
        <View title="Login" style={styles.login}>
            <Text style={styles.title}>Login</Text>
            <Button
                title="Login"
                color="purple"
                style={styles.button}
                onPress={() => login()}
            />
        </View>
    }

    <View title="Register" style={styles.register}>
    <Text style={styles.title}>Register</Text>
    <Text style={styles.label}>Prénom</Text>
    <TextInput
        id="prenom"
        style={styles.label}
        variant="filled"
        onChangeText={text => setPrenom(text)}
        value={prenom}
        placeholder="Prénom"  />
    <Text style={styles.label}>Nom</Text>
    <TextInput id="nom"
        style={styles.label}
        variant="filled"
        onChangeText={text => setNom(text)}
        value={nom}
        placeholder="Nom"/>
      <Button
                title="Register"
                color="purple"
                style={styles.button}
                onPress={() => register()}
      />
    </View>
    <StatusBar style="auto" />
    </View>
  );
}

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    user: {
      position: "absolute",
      top: 50,
      left: 30,
    },
    login: {
      width: '100%',
      marginVertical: 50,
    },
    register: {
      width: '100%',
    },
    title: {
      textAlign: 'center',
      fontSize: 20
    },
    label: {
      marginLeft: 30
    },
  })